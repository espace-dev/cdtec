# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import argparse
import multiprocessing as mp
import os
import re

from pyrasta.raster import Raster
from tqdm import tqdm

from cdtec.base import single_change_detection, multi_change_detection, change_intensity, \
    change_tendency, count_nb_changes
from cdtec.main import cdtec_run


MIN_NB_PROCESSES = 1
MAX_NB_PROCESSES = mp.cpu_count()


FHANDLE = dict(single=single_change_detection,
               multi=multi_change_detection,
               intensity=change_intensity,
               tendency=change_tendency,
               count=count_nb_changes)

DATA_TYPE = dict(single="int32",
                 multi="int32",
                 intensity="float32",
                 tendency="float32",
                 count="int16")


def _int(string):
    try:
        value = int(string)
    except ValueError:
        raise argparse.ArgumentTypeError("Value must be an integer")
    else:
        return value


def directory(value):
    if not os.path.isdir(value):
        raise argparse.ArgumentTypeError(f"'{value}' is not a valid directory path")

    return value


def percent_float(string):
    value = float(string)
    if value < 0 or value > 1:
        raise argparse.ArgumentTypeError("Value must be between 0 and 1")

    return value


def positive_int(string):
    value = _int(string)
    if value <= 0:
        raise argparse.ArgumentTypeError("Value must be strictly positive")

    return value


def nb_processes_range(string):
    value = _int(string)
    if value < MIN_NB_PROCESSES or value > MAX_NB_PROCESSES:
        raise argparse.ArgumentTypeError(f"Value must be between {MIN_NB_PROCESSES} and {MAX_NB_PROCESSES}")

    return value


parser = argparse.ArgumentParser()
parser.add_argument("InputDir", type=directory,
                    help="Input directory where to read images")
parser.add_argument("OutputDir", type=directory,
                    help="Output directory where to write output image(s)")
parser.add_argument("-method", type=str, default="single",
                    choices=['single', 'multi', 'intensity', 'tendency', 'count'],
                    help="Which method to be used in running change detection")
parser.add_argument("-c_levels", type=percent_float, nargs='+', required=True,
                    help="Confidence levels for CuSum bootstrapping")
parser.add_argument("-prefix", type=str, default="",
                    help="Prefix for output image name(s)")
parser.add_argument("-max_samples", type=positive_int, default=500,
                    help="Maximum number of samples used in CuSum bootstrapping")
parser.add_argument("-no_data", type=int, default=0,
                    help="No data value in output raster image(s)")
parser.add_argument("-nb_processes", type=nb_processes_range, default=mp.cpu_count()//2,
                    help="Number of processes for multiprocessing")
parser.add_argument("-window_size", type=int, default=50,
                    help="Window size for multiprocessing")
parser.add_argument("-chunksize", type=int, default=1,
                    help="Chunk size used in multiprocessing options")


def main(args):
    """ Run cdtec_run function
    
    Parameters
    ----------
    args

    Returns
    -------

    """
    prefix = args.prefix

    if prefix:
        prefix = prefix + "_"

    images = []
    dates = []

    for _, _, file in os.walk(args.InputDir):
        for name in file:
            if not name.startswith("."):
                images.append(os.path.join(args.InputDir, name))
                dates.append(int(re.search(r'_(\d{8})T', name)[1]))

    images.sort()

    sources = [Raster(img) for img in images]

    for c_level in tqdm(args.c_levels, total=len(args.c_levels), desc="Change detection"):
        result = cdtec_run(FHANDLE[args.method], sources, dates, c_level,
                           args.max_samples, args.nb_processes,
                           args.window_size, args.chunksize, DATA_TYPE[args.method],
                           args.no_data, progress_bar=True)

        result.to_file(os.path.join(args.OutputDir,
                                    "%s%s_%d.tif" % (prefix,
                                                     args.method,
                                                     c_level * 100)))

    return 0


if __name__ == '__main__':
    input_args = parser.parse_args()
    main(input_args)
