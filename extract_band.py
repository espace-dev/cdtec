# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os

from pyrasta.raster import Raster

img_dir = "/media/benjamin/storage/Test_Para/Zone_etude_1/13/20180101_20190101/Mask/bilateral7"
vh_dir = "/media/benjamin/storage/Test_Para/Zone_etude_1/13/20180101_20190101/VH"
vv_dir = "/media/benjamin/storage/Test_Para/Zone_etude_1/13/20180101_20190101/VV"

for _, _, file in os.walk(img_dir):
    for name in file:
        if not name.startswith("."):
            vh_raster = Raster(os.path.join(img_dir, name)).extract_bands([1])
            vv_raster = Raster(os.path.join(img_dir, name)).extract_bands([2])

            vh_raster.to_file(os.path.join(vh_dir, name))
            vv_raster.to_file(os.path.join(vv_dir, name))
