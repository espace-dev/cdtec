import numpy as np
cimport numpy as np
cimport cython

DTYPE = np.float32

ctypedef np.float32_t DTYPE_t


@cython.boundscheck(False) # Turn off bounds-checking for entire function 
@cython.wraparound(False) # Turn off negative index wrapping for entire function
def single_change(int nb_samples, float threshold, np.ndarray[DTYPE_t, ndim=3] arrays
):

    cdef np.ndarray[DTYPE_t, ndim=2] avg = np.mean(arrays, axis=0)
    cdef np.ndarray[DTYPE_t, ndim=3] arrays_minus_avg = arrays - avg
    cdef np.ndarray[DTYPE_t, ndim=3] cumsum = np.cumsum(arrays_minus_avg, axis=0)
    cdef np.ndarray[DTYPE_t, ndim=2] max_cumsum = np.max(cumsum, axis=0)
    cdef np.ndarray[DTYPE_t, ndim=2] min_cumsum = np.min(cumsum, axis=0)
    cdef np.ndarray[DTYPE_t, ndim=2] rg = max_cumsum - min_cumsum

    cdef int x = avg.shape[0]
    cdef int y = avg.shape[1]
    cdef float is_change = 1.
    cdef float ratio_nb_samples = 1/nb_samples

    cdef np.ndarray[DTYPE_t, ndim=2] ci = np.ones([x, y], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=2] change = np.zeros([x, y], dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=3] diff_bstrp_avg
    cdef np.ndarray[DTYPE_t, ndim=3] cumsum_bs
    cdef np.ndarray[DTYPE_t, ndim=2] rg_bs
    cdef np.ndarray[DTYPE_t, ndim=2] max_cumsum_bs, min_cumsum_bs

    cdef np.ndarray[DTYPE_t, ndim=3] bootstrap = np.random.permutation(arrays)

    for _ in range(nb_samples):
        diff_bstrp_avg = bootstrap - avg
        cumsum_bs = np.cumsum(diff_bstrp_avg, axis=0)
        max_cumsum_bs = np.max(cumsum_bs, axis=0)
        min_cumsum_bs = np.min(cumsum_bs, axis=0)
        rg_bs = max_cumsum_bs - min_cumsum_bs

        ci[rg_bs > rg] -= ratio_nb_samples

        np.random.shuffle(bootstrap)

    change[ci >= threshold] = is_change

    return change
