# CDTEC installer

Install CDTEC scripts

## Installation

You may download and install cdtec scripts (cdtec_run, etc.) 
by using the installer through the following commands in a terminal :

```bash
$ wget https://framagit.org/benjaminpillot/change_detection/raw/master/cdtec-installer/bin/cdtec-installer && bash cdtec-installer
```

## GDAL
`gdal 3.0.4` must be installed on your machine for CDTEC scripts to run correctly.

## How to

### cdtec_run

```bash
cdtec_run /path/to/your/input/images /path/to/output/image -c_levels .95 .99
```

**Positional arguments**
1. _Input directory_: path to directory where input images are stored
2. _Output directory_: path to directory where output image(s) must be written

**Required optional argument**
* `c_levels`: (float between 0 and 1) bootstrapping confidence level(s) for which change detection must be computed
  
**Optional arguments**
* `method`: (str) which kind of change detection method to be run {single, multi, intensity, tendency, count}
* `max_samples`: (int) maximum number of samples used in CuSum bootstrapping
* `no_data`: (int) no data value in output raster image(s) 
* `nb_processes`: (int) number of processes used in multiprocessing
* `window_size`: (int) window size used in multiprocessing
* `chunksize`: (int) chunk size used in multiprocessing options
* `prefix`: (str) prefix for output image(s)

see `cdtec_run --help` for more information