# Change detection in satellite images

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://framagit.org/benjaminpillot/change_detection/activity)
[![PyPI version fury.io](https://badge.fury.io/py/cdtec.svg)](https://pypi.python.org/pypi/cdtec/)

## List of authors
* Benjamin Pillot <benjamin.pillot@ird.fr>
* Antoine Pfefer <pfeferantoine@gmail.com>
* Bertrand Ygorra <bertrand.ygorra@inrae.fr>
* Frédéric Frappart <frederic.frappart@legos.obs-mip.fr>
* Thibault Catry <thibault.catry@ird.fr>

## Description
(coming soon)


## Installation
See [here](cdtec-installer/README.md)

<br/>

![image](docs/espace-dev-ird.png)