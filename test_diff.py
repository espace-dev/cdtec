# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os.path

import gdal
from pyrasta.raster import Raster

import numpy as np


def binarize(array):
    boolean = np.zeros(array.shape)
    boolean[array > 0] = 1

    return boolean


def diff(arrays):
    return binarize(arrays[0]) - binarize(arrays[1])


zone_1_2018 = "/media/benjamin/storage/RADAR/Z1_2018/CHANGE/LINEAR"
zone_1_2017 = "/media/benjamin/storage/RADAR/Z1_2017/CHANGE/LINEAR"
zone_2_2018 = "/media/benjamin/storage/RADAR/Z2_2018/CHANGE/LINEAR"
zone_2_2017 = "/media/benjamin/storage/RADAR/Z2_2017/CHANGE/LINEAR"

output_dir = "/media/benjamin/storage/RADAR/Z1_DIFF_2017_2018/"


raster_2018 = Raster(os.path.join(zone_1_2018, "VV", "single_99.tif"))
raster_2017 = Raster(os.path.join(zone_1_2017, "VV", "single_99.tif"))

change_zone_1 = Raster.raster_calculation([raster_2018, raster_2017], diff,
                                          output_type=gdal.GetDataTypeByName("Int16"))

change_zone_1.to_file(os.path.join(output_dir, "VV", "single_99.tif"))
