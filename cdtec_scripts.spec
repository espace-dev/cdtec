# -*- mode: python ; coding: utf-8 -*-

import os
import sys

block_cipher = None


a = Analysis(['cdtec_run.py'],
             pathex=['.'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

b = Analysis(['cdtec_convert.py'],
             pathex=['.'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

MERGE((b, 'cdtec_convert', os.path.join('cdtec_convert', 'cdtec_convert')),
      (a, 'cdtec_run', os.path.join('cdtec_run', 'cdtec_run')))


pyz = PYZ(a.pure, a.zipped_data,
          cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.dependencies,
          [],
          exclude_binaries=True,
          name=os.path.join('build', 'pyi.' + sys.platform, 'cdtec_run',
                            'cdtec_run'),
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None)


coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas, 
               strip=False,
               upx=True,
               upx_exclude=[],
               name=os.path.join('dist', 'cdtec_run'))


pyzB = PYZ(b.pure)

exeB = EXE(pyzB,
           b.scripts,
           b.dependencies,
           exclude_binaries=1,
           name=os.path.join('build', 'pyi.' + sys.platform, 'cdtec_convert',
                             'cdtec_convert'),
           debug=True,
           strip=False,
           upx=True,
           console=1)

coll = COLLECT(exeB,
               b.binaries,
               b.zipfiles,
               b.datas,
               strip=False,
               upx=True,
               name=os.path.join('dist', 'cdtec_convert'))
