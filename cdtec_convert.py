# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import argparse
import os

from pyrasta.raster import Raster
from tqdm import tqdm


def directory(value):
    if not os.path.isdir(value):
        raise argparse.ArgumentTypeError(f"'{value}' is not a valid directory path")

    return value


parser = argparse.ArgumentParser()
parser.add_argument("InputDir", type=directory,
                    help="Input directory where images must be read from")
parser.add_argument("OutputDir", type=directory,
                    help="Output directory where output image(s) will be write to")
parser.add_argument("-method", type=str, default="log2linear",
                    choices=['log2linear', 'linear2log'],
                    help="Which method to be used in conversion")


def log_to_linear(image):
    return 10 ** (Raster(image) / 10)


def linear_to_log(image):
    return 10 * Raster(image).log10()


def main(args):

    if args.InputDir == args.OutputDir:
        parser.error(f"Input directory ('{args.InputDir}') must be different "
                     f"from output directory (f'{args.OutputDir}')")
        # return 1

    if args.method == 'log2linear':
        converter = log_to_linear
        method = "log to linear"
    elif args.method == 'linear2log':
        converter = linear_to_log
        method = "linear to log"
    else:
        converter = Raster
        method = None

    nb_files = sum([len(files) for _, _, files in os.walk(args.InputDir)])
    images = "image%s" % ("s" if nb_files > 1 else "")
    pg_bar = tqdm(desc=f"Converting {nb_files} {images} ({method})",
                  total=nb_files)

    for _, _, files in os.walk(args.InputDir):
        for name in files:
            if not name.startswith("."):
                img = os.path.join(args.InputDir, name)
                raster = converter(img)
                raster.to_file(os.path.join(args.OutputDir, name))

            pg_bar.update(1)

    pg_bar.close()

    return 0


if __name__ == "__main__":
    input_args = parser.parse_args()
    main(input_args)
